﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AR_Lab7_8
{
    class Graph
    {


        private string[,] cities;
        public string[,] Cities
        {
            get
            {
                return cities;
            }
            set
            {
                cities = value;
            }
        }

        private int[,] adjMatrix;
        public int[,] AdjMatrix
        {
            get
            {
                return adjMatrix;
            }
            set
            {
                adjMatrix = value;
            }
        }
    }
}
