﻿using System;
using System.Diagnostics;
using System.Text;

namespace AR_Lab6
{
    class Program
    {
        public static void PrintArray(short[] arr)
        {
            foreach (short item in arr)
            {
                Console.WriteLine($"\t{item}");
            }
        }
        public static void PrintArray(float[] arr)
        {
            foreach (float item in arr)
            {
                Console.WriteLine($"\t{item}");
            }
        }
        public static void PyramidSort(float[] arr)
        {
            int i, j, childIndex, root;
            
            float temp;

            //строим кучку так, чтобы каждый узловой элемент был максимальным для дочерних

            for (i = 1; i < arr.Length; i++) //от 1, потому что индекс дочернего элемента не может быть 0
            {
                childIndex = i;

                //выполнять пока индекс дочернего элемента != 0
                do
                {
                    root = (childIndex - 1) / 2;
                    if (arr[root] < arr[childIndex])
                    {
                        temp = arr[root];
                        arr[root] = arr[childIndex];
                        arr[childIndex] = temp;
                    }
                    //перезаписываем индекс дочернего элемента на
                    //индекс коренного, чтобы либо запустить проверку в обратку,
                    //либо прекратить цикл
                    childIndex = root;
                } while (childIndex != 0);
            }

            //просеиваем кучку и сортируем её
            for (j = arr.Length - 1; j >= 0; j--)
            {
                //меняем местами нулевой элемент (который у нас максимальный для всей кучки) и элемент j
                //на каждой итерации. Таким образом
                //максимальные элементы идут в конец и не рассматриваются в цикле
                temp = arr[0];
                arr[0] = arr[j];
                arr[j] = temp;
                root = 0;

                //дочерний элемент не должен стать j
                do
                {
                    //ищем дочерний элемент
                    childIndex = 2 * root + 1;
                    // < j - 1 для того, чтобы не трогать уже просеянные элементы (j в конец же уходит)

                    if (childIndex < j - 1)
                    {
                        if ((arr[childIndex] < arr[childIndex + 1]))
                        //мы проверяем, или дочерний элемент меньше соседнего дочернего, потому что на верху нам нужен максимальный
                        {
                            childIndex++;
                        }
                    }

                    if (childIndex < j)
                    {
                        //индекс дочернего < j (чтобы не выходило за границы массива)
                        if (arr[root] < arr[childIndex])
                        {
                            temp = arr[root];
                            arr[root] = arr[childIndex];
                            arr[childIndex] = temp;
                        }
                    }

                    //индекс коренного равен дочернему
                    //тогда если проверяется элемент 1, то коренной для него будет 0.
                    //элемент 1 станет локальным коренным и мы будем проверять то, что под ним
                    //например, элементов 3 и 4. И так до тех пор, пока индекс не упрётся в j
                    root = childIndex;
                } while (childIndex < j);
            }

        }

        public static void ShellSort(float[] arr)
        {

            int step = arr.Length / 2;
            float temp;

            while (step <= arr.Length / 2)
                step = (3 * step - 1);

            //shell sort

            while (step > 0)
            {
                for (int i = 0; i < (arr.Length - step); i++)
                //this loop controls the boundaries of the sort
                {
                    int j = i;
                    while (j >= 0 && arr[j] > arr[j + step]) //compares the ellements
                    {
                        temp = arr[j];
                        arr[j] = arr[j + step];
                        arr[j + step] = temp;
                        j--; //is needed on the last stage of the sort, when step == 1 to move
                             //unsorted elements and basically make the insertion sort
                    }
                }
                step /= 2;
            }
        }

        public static short FindMin(short[] arr)
        {
            short min = short.MaxValue;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }
            }
            return min;
        }

        public static short FindMax(short[] arr)
        {
            short max = short.MinValue;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            return max;
        }

        public static void CountingSort(short[] arr)
        {
            short min = arr[0], max = arr[0];
            min = FindMin(arr);
            max = FindMax(arr);

            short[] timesORepeating = new short[max - min + 1];

            int j = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                timesORepeating[arr[i] - min]++;
            }
            for (short i = min; i <= max; i++)
            {
                while (timesORepeating[i - min] > 0)
                {
                    arr[j] = i; 
                    j++;
                    timesORepeating[i - min]--;
                }
            }
            
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Stopwatch watch;

            int menu;

            Console.Write("\n\n\tВведіть розмір масиву для пірамідального сортування: ");
            int sizeOfArray = Int32.Parse(Console.ReadLine());

            float[] pyramidArr = new float[sizeOfArray];

            for (int i = 0; i < pyramidArr.Length; i++)
            {
                pyramidArr[i] = (float)(Math.Round(new Random().NextDouble() * (10 + 200) - 200, 2));
            }

            PrintArray(pyramidArr);

            Console.Write("\n\n\tВведіть розмір масиву для сортування Шелла: ");
            sizeOfArray = Int32.Parse(Console.ReadLine());

            float[] shellArr = new float[sizeOfArray];

            for (int i = 0; i < shellArr.Length; i++)
            {
                shellArr[i] = (float)(Math.Round(new Random().NextDouble() * (100 + 10) - 10, 2));
            }

            PrintArray(shellArr);

            Console.Write("\n\n\tВведіть розмір масиву для сортування підрахунком: ");
            sizeOfArray = Int32.Parse(Console.ReadLine());

            short[] countArr = new short[sizeOfArray];

            for (int i = 0; i < countArr.Length; i++)
            {
                countArr[i] = (short)(new Random().Next(-10, 40));
            }

            PrintArray(countArr);

            do
            {
                Console.WriteLine($"\n\t0 - завершити роботу" +
                    "\n\t1 - згенерувати новий масив для пірамідального сортування" +
                    "\n\t2 - згенерувати новий масив сортування Шелла" +
                    "\n\t3 - згенерувати новий масив сортування підрахунком" +
                    "\n\t4 - пірамідальне сортування" +
                    "\n\t5 - сортування Шелла" +
                    "\n\t6 - сортування підрахунком");
                Console.Write("\n\n\tОберіть дію: ");
                menu = Int32.Parse(Console.ReadLine());

                switch (menu)
                {
                    case 0:
                        {
                            break;
                        }
                    case 1:
                        {
                            Console.Write("\n\n\tВведіть розмір масиву для пірамідального сортування: ");
                            sizeOfArray = Int32.Parse(Console.ReadLine());

                            pyramidArr = new float[sizeOfArray];

                            for (int i = 0; i < pyramidArr.Length; i++)
                            {
                                pyramidArr[i] = (float)(Math.Round(new Random().NextDouble() * (10 + 200) - 200, 2));
                            }

                            PrintArray(pyramidArr);

                            break;
                        }
                    case 2:
                        {
                            Console.Write("\n\n\tВведіть розмір масиву для сортування Шелла: ");
                            sizeOfArray = Int32.Parse(Console.ReadLine());

                            shellArr = new float[sizeOfArray];

                            for (int i = 0; i < shellArr.Length; i++)
                            {
                                shellArr[i] = (float)(Math.Round(new Random().NextDouble() * (100 + 10) - 10, 2));
                            }

                            PrintArray(shellArr);

                            break;
                        }
                    case 3:
                        {
                            Console.Write("\n\n\tВведіть розмір масиву для сортування підрахунком: ");
                            sizeOfArray = Int32.Parse(Console.ReadLine());
                            countArr = new short[sizeOfArray];

                            for (int i = 0; i < countArr.Length; i++)
                            {
                                countArr[i] = (short)(new Random().Next(-10, 40));
                            }

                            PrintArray(countArr);

                            break;
                        }
                    case 4:
                        {
                            watch = Stopwatch.StartNew();
                            PyramidSort(pyramidArr);
                            watch.Stop();

                            PrintArray(pyramidArr);

                            Console.WriteLine($"\n\tЧас сортування для {pyramidArr.Length} елементів: {watch.Elapsed.TotalMilliseconds}ms");

                            break;
                        }
                    case 5:
                        {
                            watch = Stopwatch.StartNew();
                            ShellSort(shellArr);
                            watch.Stop();

                            PrintArray(shellArr);

                            Console.WriteLine($"\n\tЧас сортування для {shellArr.Length} елементів: {watch.Elapsed.TotalMilliseconds}ms");

                            break;
                        }
                    case 6:
                        {
                            watch = Stopwatch.StartNew();
                            CountingSort(countArr);
                            watch.Stop();

                            PrintArray(countArr);

                            Console.WriteLine($"\n\tЧас сортування для {countArr.Length} елементів: {watch.Elapsed.TotalMilliseconds}ms");


                            break;
                        }
                }


            } while (menu != 0);


        }

    }
}
