﻿#include <iostream>
#include <windows.h>
#include <chrono>
#include <time.h>
#define _CRT_SECURE_NO_WARNINGS
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
#define SIZE 40

long long CastToDecimal(int arr[])
{
    long long num = 0;
    for (int i = 0; i < SIZE; i++)
    {
        num += arr[i] * pow(2, SIZE - 1 - i);
    }
    return num;
}

int* GetMax(int arr[])
{
    int check = 1;
    do
    {
        check = 0;

        for (int i = 0; i < SIZE; i++)
        {
            if (arr[i] == 0 && arr[i + 1] == 1)
            {
                arr[i] = 1;
                arr[i + 1] = 0;
                check = 1;
            }
            else if (arr[i] == 1 && arr[i - 1] == 0 && i - 1 != 0)
            {
                arr[i] = 0;
                arr[i - 1] = 1;
                check = 1;
            }
        }
    } while (check);

    return arr;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    srand(time(0));
    printf("\n\n\tЧисло у двійковій системі: ");
    int arr[SIZE];
    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = 0 + rand() % (1 - 0 + 1);
        printf("%d", arr[i]);
    }

    printf("\n\n\tЧисло в десятковій системі: %lld\n\n\t", CastToDecimal(arr));
    auto begin = GETTIME();

    GetMax(arr);

    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);

    printf("\n\n\tМаксимальне число у двійковій системі: ");
    for (int i = 0; i < SIZE; i++)
    {
        printf("%d", arr[i]);
    }
    printf("\n\n\tМаксимальне число у десятковій системі: %lld", CastToDecimal(arr));

    printf("\n\n\tЧас виконання: %lld нс\n\n", elapsed_ns.count());

    return 0;
}