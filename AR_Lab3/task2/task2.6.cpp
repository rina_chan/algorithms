﻿#include <iostream>
#include <windows.h>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

int Fibonacci(int n)
{
    if (n == 1 || n == 2)
    {
        return 1;
    }
    return Fibonacci(n - 1) + Fibonacci(n - 2);
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int n;
    printf("\n\tВведіть n для знаходження n-ного числа Фібоначчі (n <= 40): ");
    scanf_s("%d", &n);
    printf("\n");

    if (n > 40)
    {
        printf("\n\tn має бути <= 40");
    }
    else
    {
        auto begin = GETTIME();
        int fibonacciNum = Fibonacci(n);
        auto end = GETTIME();
        auto elapsed_ns = CALCTIME(end - begin);
        printf("\n\tВаше число: %d", fibonacciNum);

        printf("\n\tЧас виконання: %lld нс", elapsed_ns.count());
    }
    printf("\n\n");

    return 0;
}