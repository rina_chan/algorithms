﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AR_Lab5
{
    class Program
    {
        public static void SelectionSort(LinkedList<int> list)
        {
            LinkedListNode<int> currentIfMin = list.First;
            LinkedListNode<int> current = list.First;
            LinkedListNode<int> min;
            int temp;

            while (current != null)
            {
                min = current;
                currentIfMin = current.Next;

                //ищем минимальный элемент

                while (currentIfMin != null)
                {
                    if (currentIfMin.Value < min.Value)
                    {
                        min = currentIfMin;
                    }
                    currentIfMin = currentIfMin.Next;
                }

                temp = current.Value;
                current.Value = min.Value;
                min.Value = temp;

                current = current.Next;
            } 
        }

        public static void InsertionSortArr(ref int[] arr)
        {
            int temp;

            for (int i = 1; i < arr.Length; i++)
            {
                temp = arr[i]; //take the element out
                for (int j = i - 1; j >= 0 && arr[j] > temp; j--)
                {
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        public static void InsertionSortList(LinkedList<int> list)
        {
            LinkedListNode<int> current = list.First;
            LinkedListNode<int> nodeForSort;
            LinkedListNode<int> placeForNode;
            int tempValue;

            while (current != null)
            {
                //ищем минимум перед текущим элементом
                if (current.Previous == null)
                {
                    current = current.Next;
                }
                //чтобы не нарушить адрес текущего элемента, сохраняем указатель на его
                //в отдельную переменную
                nodeForSort = current;
                //эта переменная будет хранить какую-то из тех,
                //которые находятся перед сортируемым элементом
                placeForNode = nodeForSort.Previous;
                //пока не достигнем начала списка
                //и пока сортируемый элемент меньше предыдущего
                while (placeForNode != null && nodeForSort.Value < placeForNode.Value)
                {
                    //мы можем его двигать
                    //т.е. менять местами с предыдущими элементами
                    tempValue = placeForNode.Value;
                    placeForNode.Value = nodeForSort.Value;
                    nodeForSort.Value = tempValue;

                    //но это не сортировка пузырьком
                    //мы не можем себе позволить заново запускать цикл
                    //поэтому просто запоминаем адрес места, где сейчас
                    //находится проверяемый элемент
                    //а он находится в одной из предыдущих ячеек
                    nodeForSort = placeForNode;
                    placeForNode = placeForNode.Previous;
                }

                current = current.Next;
            }
        }

        public static void PrintList(LinkedList<int> list)
        {
            foreach (int item in list)
            {
                Console.WriteLine($"\t{item}");
            }
        } 

        public static void PrintArray(int[] arr)
        {
            foreach (int item in arr)
            {
                Console.WriteLine($"\t{item}");
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Stopwatch watch;

            int menu;

            LinkedList<int> list = new LinkedList<int>();

            Console.Write("\n\n\tВведіть розмір масиву: ");
            int sizeOfArray = Int32.Parse(Console.ReadLine());

            int[] arr = new int[sizeOfArray];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = new Random().Next(0, 30);
            }

            PrintArray(arr);

            Console.Write("\n\n\tВведіть розмір списку: ");
            int sizeOfList = Int32.Parse(Console.ReadLine());

            for (int i = 0; i < sizeOfList; i++)
            {
                list.AddLast(new Random().Next(0, 30));
            }

            PrintList(list);

            do
            {
                Console.WriteLine($"\n\t0 - завершити роботу" +
                    "\n\t1 - згенерувати новий масив" +
                    "\n\t2 - згенерувати новий двузв'язний список" +
                    "\n\t3 - сортування вибором (двузв'язний список)" +
                    "\n\t4 - сортування вставками (масив)" +
                    "\n\t5 - сортування вставками (двузв'язний список)");
                Console.Write("\n\n\tОберіть дію: ");
                menu = Int32.Parse(Console.ReadLine());

                switch (menu)
                {
                    case 0:
                        {
                            break;
                        }
                    case 1:
                        {
                            Console.Write("\n\n\tВведіть розмір масиву: ");
                            sizeOfArray = Int32.Parse(Console.ReadLine());

                            arr = new int[sizeOfArray];

                            for (int i = 0; i < arr.Length; i++)
                            {
                                arr[i] = new Random().Next(0, 30);
                            }

                            PrintArray(arr);

                            break;
                        }
                    case 2:
                        {
                            Console.Write("\n\n\tВведіть розмір списку: ");
                            sizeOfList = Int32.Parse(Console.ReadLine());

                            for (int i = 0; i < list.Count; i++)
                            {
                                list.Clear();
                            }

                            for (int i = 0; i < sizeOfList; i++)
                            {
                                list.AddLast(new Random().Next(0, 30));
                            }

                            PrintList(list);

                            break;
                        }
                    case 3:
                        {
                            watch = Stopwatch.StartNew();
                            SelectionSort(list);
                            watch.Stop();

                            PrintList(list);

                            Console.WriteLine($"\n\tЧас сортування для {list.Count} елементів: {watch.Elapsed.TotalMilliseconds}ms");

                            break;
                        }
                    case 4:
                        {
                            watch = Stopwatch.StartNew();
                            InsertionSortArr(ref arr);
                            watch.Stop();

                            PrintArray(arr);

                            Console.WriteLine($"\n\tЧас сортування для {arr.Length} елементів: {watch.Elapsed.TotalMilliseconds}ms");

                            break;
                        }
                    case 5:
                        {
                            watch = Stopwatch.StartNew();
                            InsertionSortList(list);
                            watch.Stop();

                            PrintList(list);

                            Console.WriteLine($"\n\tЧас сортування для {list.Count} елементів: {watch.Elapsed.TotalMilliseconds}ms");


                            break;
                        }
                }


            } while (menu != 0);

            
        }
           
    }
}
