﻿#include <iostream>
#include <windows.h>
#include <time.h>
#include <string.h>

union CompactFloat
{
    float a;
    struct
    {
        struct
        {
            unsigned char b0 : 1;
            unsigned char b1 : 1;
            unsigned char b2 : 1;
            unsigned char b3 : 1;
            unsigned char b4 : 1;
            unsigned char b5 : 1;
            unsigned char b6 : 1;
            unsigned char b7 : 1;
        } byte0Var;
        struct
        {
            unsigned char b8 : 1;
            unsigned char b9 : 1;
            unsigned char b10 : 1;
            unsigned char b11 : 1;
            unsigned char b12 : 1;
            unsigned char b13 : 1;
            unsigned char b14 : 1;
            unsigned char b15 : 1;

        } byte1Var;
        struct
        {
            unsigned char b16 : 1;
            unsigned char b17 : 1;
            unsigned char b18 : 1;
            unsigned char b19 : 1;
            unsigned char b20 : 1;
            unsigned char b21 : 1;
            unsigned char b22 : 1;
            unsigned char b23 : 1;
        } byte2Var;
        struct
        {
            unsigned char b24 : 1;
            unsigned char b25 : 1;
            unsigned char b26 : 1;
            unsigned char b27 : 1;
            unsigned char b28 : 1;
            unsigned char b29 : 1;
            unsigned char b30 : 1;
            unsigned char b31 : 1;
        } byte3Var; 
    } b;
};

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    CompactFloat num;

    num.a = 16.101;
    printf("\n\tЗначення побітово:");
    printf("\n\tЧисло a = %f = 0b%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", num.a, num.b.byte3Var.b31, num.b.byte3Var.b30, num.b.byte3Var.b29, num.b.byte3Var.b28, num.b.byte3Var.b27, num.b.byte3Var.b26, num.b.byte3Var.b25, num.b.byte3Var.b24, num.b.byte2Var.b23, num.b.byte2Var.b22, num.b.byte2Var.b21, num.b.byte2Var.b20, num.b.byte2Var.b19, num.b.byte2Var.b18, num.b.byte2Var.b17, num.b.byte2Var.b16, num.b.byte1Var.b15, num.b.byte1Var.b14, num.b.byte1Var.b13, num.b.byte1Var.b12, num.b.byte1Var.b11, num.b.byte1Var.b10, num.b.byte1Var.b9, num.b.byte1Var.b8, num.b.byte0Var.b7, num.b.byte0Var.b6, num.b.byte0Var.b5, num.b.byte0Var.b4, num.b.byte0Var.b3, num.b.byte0Var.b2, num.b.byte0Var.b1, num.b.byte0Var.b0);
    printf("\n\tЗначення побайтово:");
    printf("\n\tЧисло a = %f = 0b%d %d %d %d\n\n", num.a, num.b.byte3Var, num.b.byte2Var, num.b.byte1Var, num.b.byte0Var);
    
    char signVar;
    if (num.b.byte3Var.b31 == 0)
    {
        signVar = '+';
    }
    else
    {
        signVar = '-';
    }
    printf("\n\tЗнак: %c", signVar);

    printf("\n\tСтупінь значення: %d%d%d%d%d%d%d%d", num.b.byte3Var.b30, num.b.byte3Var.b29, num.b.byte3Var.b28, num.b.byte3Var.b27, num.b.byte3Var.b26, num.b.byte3Var.b25, num.b.byte3Var.b24, num.b.byte2Var.b23);

    printf("\n\tМантиса: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", num.b.byte2Var.b22, num.b.byte2Var.b21, num.b.byte2Var.b20, num.b.byte2Var.b19, num.b.byte2Var.b18, num.b.byte2Var.b17, num.b.byte2Var.b16, num.b.byte1Var.b15, num.b.byte1Var.b14, num.b.byte1Var.b13, num.b.byte1Var.b12, num.b.byte1Var.b11, num.b.byte1Var.b10, num.b.byte1Var.b9, num.b.byte1Var.b8, num.b.byte0Var.b7, num.b.byte0Var.b6, num.b.byte0Var.b5, num.b.byte0Var.b4, num.b.byte0Var.b3, num.b.byte0Var.b2, num.b.byte0Var.b1, num.b.byte0Var.b0);

    printf("\n\tОб'єм пам'яті, який займає змінна: %d", sizeof(CompactFloat));


}