﻿#include <stdio.h>
#include <windows.h>

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    unsigned char x, y, z;
    signed char a, b, c;

    a = 127;
    b = 1;
    c = a + b;
    printf("signed char c=127+1=%d\n", c);

    c = a << 1;
    printf("signed char c=127<<1=%d\n", c);

    x = 5;
    y = 7;
    z = x - y;
    printf("unsigned char z=5-6=%u\n", z);
    return 0;
}
