﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <windows.h>
#include <time.h>
#include <string.h>

struct Date
{
    unsigned short Day : 5;
    unsigned short Month : 4;
    unsigned short Year : 7;

    unsigned short Hours : 5;
    unsigned short Minutes : 6;
    unsigned short Seconds : 6;
};

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    Date defaultDate;

    long long systemTime;
    struct tm* m_time;

    systemTime = time(NULL);
    m_time = localtime(&systemTime);
    char strDateTime[128] = "";
    strftime(strDateTime, 128, "%x", m_time);

    char* tokvar;
    tokvar = strtok(strDateTime, "/");
    defaultDate.Month = atoi(tokvar);
    tokvar = strtok(NULL, "/");
    defaultDate.Day = atoi(tokvar);
    tokvar = strtok(NULL, "/");
    defaultDate.Year = atoi(tokvar);


    strftime(strDateTime, 128, "%X", m_time);
    tokvar = strtok(strDateTime, ":");
    defaultDate.Hours = atoi(tokvar);
    tokvar = strtok(NULL, ":");
    defaultDate.Minutes = atoi(tokvar);
    tokvar = strtok(NULL, ":");
    defaultDate.Seconds = atoi(tokvar);

    printf("Дата: %d/%d/%d", defaultDate.Day, defaultDate.Month, defaultDate.Year + 2000);
    printf("\tЧас: %d:%d:%d\n", defaultDate.Hours, defaultDate.Minutes, defaultDate.Seconds);

    printf("Розмір структури даного типу: %d байт\n", sizeof(defaultDate));
    printf("Розмір структури tm: %d байт\n", sizeof(tm));


    return 0;
}
