﻿using System;
using System.Collections.Generic;
using System.Text;
using LinkedList;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;


            LinkedList<string> list = new LinkedList<string>();

            int menu, newMenu;

            do
            {
                Console.WriteLine("\x1b[35m\n\n\tСтворюємо список:");
                Console.WriteLine("\n\t0 - Завершити роботу" +
                    "\n\t1 - додати елемент" +
                    "\n\t2 - видалити елемент" + 
                    "\n\t3 - надрукувати список");
                Console.Write("\n\tОберіть дію: \x1b[0m");

                menu = Int32.Parse(Console.ReadLine());

                switch (menu)
                {
                    case 0:
                        {
                            break;
                        }
                    case 1:
                        {
                            do
                            { 

                                    Console.WriteLine("\x1b[35m\n\n\t0 - Завершити додавання" +
                                "\n\t1 - додати елемент на початок" +
                                "\n\t2 - додати елемент у кінець" +
                                "\n\t3 - додати елемент у якесь місце списку");
                                        Console.Write("\n\tОберіть дію: \x1b[0m");

                                newMenu = Int32.Parse(Console.ReadLine());

                                switch (newMenu)
                                {
                                    case 0:
                                        {
                                            break;
                                        }
                                    case 1:
                                        {
                                            Console.Write("\n\tВведіть рядок: ");
                                            list.AddInFront(Console.ReadLine());

                                            break;
                                        }
                                    case 2:
                                        {
                                            Console.Write("\n\tВведіть рядок: ");
                                            list.AddInTheEnd(Console.ReadLine());

                                            break;
                                        }
                                    case 3:
                                        {
                                            Console.Write("\n\tВведіть рядок: ");
                                            string input = Console.ReadLine();
                                            Console.Write("\n\tВведіть номер у списку, куди буде вставлено елемент (нумерація від 0): ");
                                            int index = Int32.Parse(Console.ReadLine());

                                            list.AddAnywhere(input, index);

                                            break;
                                        }
                                    default:
                                        {
                                            Console.WriteLine("\n\n\tТакого пункту не існує.");

                                            break;
                                        }

                                }

                            } while (newMenu != 0);

                            break;

                        }
                    case 2:
                        {
                            do
                            {

                                    Console.WriteLine("\x1b[35m\n\n\t0 - Завершити видалення" +
                                "\n\t1 - видалити елемент на початку" +
                                "\n\t2 - видалити елемент у кінці" +
                                "\n\t3 - видалити елемент у якомусь місці списку" +
                                "\n\t4 - очистити список");
                                    Console.Write("\n\tОберіть дію: \x1b[0m");

                                newMenu = Int32.Parse(Console.ReadLine());

                                switch (newMenu)
                                {
                                    case 0:
                                        {
                                            break;
                                        }
                                    case 1:
                                        {
                                            list.RemoveInFront();

                                            break;
                                        }
                                    case 2:
                                        {
                                            list.RemoveAtTheEnd();

                                            break;
                                        }
                                    case 3:
                                        {
                                            Console.Write("\n\tВведіть номер у списку, куди буде вставлено елемент (нумерація від 0): ");
                                            int index = Int32.Parse(Console.ReadLine());

                                            list.RemoveAnywhere(index);

                                            break;
                                        }
                                    case 4:
                                        {
                                            list.RemoveEverything();

                                            break;
                                        }
                                    default:
                                        {
                                            Console.WriteLine("\n\tТакого пункту не існує.");

                                            break;
                                        }

                                }

                            } while (newMenu != 0);

                            break;

                        }
                    case 3:
                        {
                            Console.WriteLine("\n\n\tВаш список:");
                            list.PrintList();

                            break;
                        }
                    default:
                        {
                            Console.WriteLine("\n\tТакого пункту не існує.");

                            break;
                        }
                }


            } while (menu != 0);

        }
    }
}
