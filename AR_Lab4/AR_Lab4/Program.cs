﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AR_Lab4
{
    class Program
    {
        public static int Priority(string oper)
        {
            switch (oper)
            {
                case "(": return 0;
                case ")": return 1;
                case "+": return 2;
                case "-": return 3;
                case "*": return 4;
                case "/": return 4;
                case "^": return 5;
                case "sqrt": return 5;
                default: return 6;
            }
        }

        public static List<string> SortingStation(string expression)
        {
            List<string> rpn = new List<string>();
            Stack<string> stack = new Stack<string>();

            string[] expressionArr = expression.Split(new char[2] { ' ', '\n' });
            double num;

            foreach (string item in expressionArr)
            {
                // Если цифра
                if (double.TryParse(item, out num))
                {
                    rpn.Add(item);
                }
                // Если оператор
                else
                {
                    if (item == "(")
                    {
                        stack.Push(item);
                    }
                    else if (item == ")")
                    {
                        string symbol = stack.Pop();
                        // достаем все операторы из стека до открывающей скобки
                        while (symbol != "(")
                        {
                            rpn.Add(symbol);
                            symbol = stack.Pop();
                        }
                    }
                    // Если другой оператор
                    else if(item.Contains("sqrt"))
                    {
                        if (stack.Count > 0)
                        {
                            // если приоритет текущего оператора меньше или равен, добавляем его в строку
                            if (Priority("sqrt") <= Priority(stack.Peek()))
                            {
                                rpn.Add(stack.Pop());
                            }
                        }
                        stack.Push("sqrt");

                        string numb = "";
                        for (int i = 0; i < item.Length; i++)
                        {
                            if (Char.IsDigit(item[i]) || item[i] == '.')
                            {
                                while (Char.IsDigit(item[i]) || item[i] == '.')
                                {
                                    numb += item[i].ToString();
                                    i++;
                                }
                            }
                        }
                        rpn.Add(numb);

                    }
                    else
                    {
                        if (stack.Count > 0)
                        {
                            if (Priority(item) <= Priority(stack.Peek()))
                            {
                                rpn.Add(stack.Pop());
                            }
                        }
                        stack.Push(item);
                    }
                }
            }

            while (stack.Count > 0)
            {
                rpn.Add(stack.Pop());
            }

            return rpn;
        }

        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Stack<double> stack = new Stack<double>();

            Console.Write("\n\tВведіть вираз (операнди розділяйте пробілами): ");
            string expression = Console.ReadLine();

            List<string> expressionList = new List<string>();
            double num;

            expressionList = SortingStation(expression);

            double result = 0;

            foreach (string item in expressionList)
            {
                if (double.TryParse(item, out num))
                {
                    stack.Push(num);
                }
                else
                {
                    switch (item)
                    {
                        case "+":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 + num1);
                                break;
                            }
                        case "-":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 - num1);
                                break;
                            }
                        case "*":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 * num1);
                                break;
                            }
                        case "/":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 / num1);
                                break;
                            }
                        case "^":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(Math.Pow(num2, num1));
                                break;
                            }
                        case "sqrt":
                            {
                                stack.Push(Math.Sqrt(stack.Pop()));
                                break;
                            }
                    }
                }

            }

            result = stack.Peek();

            Console.WriteLine($"\n\n\tРезультат: {result}");

        }
    }
}