﻿#include <iostream>
#include <windows.h>
#include <time.h>

long long CustomRand(long long seed = 0)
{
    long long m = 1LL << 31;
    long long a = 1103515245;
    long long c = 12345;

    static long long previousX = 0;
    if (seed != 0)
    {
        previousX = seed;
    }

    long long x = (a * previousX + c) % m;

    previousX = x;
    return x;
}

double expectedValueFunc(long long randomArray[])
{
    double expectedValue = 0;
    int count = 0;
    double staticProbability;
    for (int i = 0; i < 9999; i++)
    {
        if (randomArray[i] == randomArray[i + 1])
        {
            count++;
        }
        else
        {
            staticProbability = count / 10000.0;
            expectedValue += randomArray[i] * staticProbability;

            count = 0;
        }


    }
    return expectedValue;
}

double dyspersionFunc(long long randomArray[])
{
    double dyspersion = 0;
    int count = 0;
    double staticProbability;
    for (int i = 0; i < 9999; i++)
    {
        if (randomArray[i] == randomArray[i + 1])
        {
            count++;
        }
        else
        {
            staticProbability = count / 10000.0;
            dyspersion += pow((randomArray[i] - expectedValueFunc(randomArray)),2) * staticProbability;
            count = 0;
        }


    }
    return dyspersion;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    long long rangeStart = 0;
    long long rangeFinish = 300;
    long long randomArray[10000];

    CustomRand(48600);
    for (int i = 0; i < 9999; i++)
    {
        randomArray[i] = CustomRand() % 300;
    }

    int check;

    long long temp;
    do
    {
        check = 0;
        for (int i = 0; i < 9998; i++)
        {
            if (randomArray[i] > randomArray[i + 1])
            {
                temp = randomArray[i + 1];
                randomArray[i + 1] = randomArray[i];
                randomArray[i] = temp;
                check = 1;
            }
        }
    } while (check);




    
    ////математичне сподівання
    double expectedValue;
    double dyspersion;
    double staticProbability;
    int count = 0;

    for (int i = 0; i < 9999; i++)
    {
        while (randomArray[i] == randomArray[i+1])
        {
            count++;
            i++;
        }
        staticProbability = count / 10000.0;
        printf("%d -- частота генерації: %d, статична імовірність: %lf\n", randomArray[i], count, staticProbability);
        count = 0;
    }

    expectedValue = expectedValueFunc(randomArray);
    dyspersion = dyspersionFunc(randomArray);
    printf("\n\nМатематичне сподівання: %lf\n", expectedValue);
    printf("Дисперсія: %lf\n", dyspersion);
    printf("Середньоквадратичне відхилення: %lf\n\n", sqrt(dyspersion));
    
    return 0;
}